## Introduction

A $\texttt{COMBI}$ simulation requires 4 input files.
The naming convention to separate these files is through distinct suffixes:

  - XXX.in - **Configuration file**: Important parameters for the simulation.
  - XXX.B1 - **Beam file for beam 1**: What bunch slots are filled in beam 1.
  - XXX.B2 - **Beam file for beam 2**: What bunch slots are filled in beam 2.
  - XXX.coll - **Action file**: What action (physics module) is calculated at which position.



## Configuration file

<!-- **Change to "input convention" and "example"?** -->

The configuration file consists of several lines of independent inputs.
The file is parsed by comparing the beginning of each line (up to ':') to a set of strings.

It can be useful to comment out a line, without having to delete it. To comment out a line, start it with two backslashes (as in $\texttt{C}$):

    //walltime: 1

If a specific parameter is not set in the file, a default value will in many cases be used.
That is however not the case for all parameters. Some parameters have to be set in the input file. (?)

The possible inputs in the configuration file are (not all are needed):
```
collision: <path.coll>
//      Path to the action file.        
//      No default.
filling: <path.B1> <path.B2>
//      Path to the beam files for both beams.
//      No default.
number of turns: <turns>
//      Number of turns to be tracked.
//      No default.
nb of macro particle: <integer>
//      Number of macro particles per numerical bunch.
//      No default.
bunch intensity: <N1> <N2>
//      Number of elementary charges per bunch in "reality".
//      Default: 1.15e11 1.15e11
energy: <E1> <E2>
//      Particle energy of both bunches [GeV].
//      Default: 7000.0 7000.0
emittance B1: <Hor. emittance> <Ver. emittance>
//      Normalized emittances of beam 1 in [mum]. Horizontal (and Vertical if different)
//      Default: 3.75
emittance B2: <Hor. emittance> <Ver. emittance>
//      Normalized emittances of beam 1 in [mum]. Horizontal (and Vertical if different)
//      Default: 3.75
beta*: <Hor. beta*> <Ver. beta*>
//      Value of transverse beta-functions at interaction point in [m].
//      Default: 0.55 0.55
mass:   <M1> <M2>
//      Particle mass in units of [GeV] of both beams (if different)
//      Default: 0.938272
charge: <Q1> <Q2>
//      Particle charge [nb of elementary charge] in both beams (if different).
//      Default: 1
bunch length: <sigma_z>
//      RMS bunchlength in [ns]
//      Default: 1.0
relative momentum spread: <sigma_dpp>
//      RMS relative momentum spread
//      Default: 1e-4
bunch spacing: <tsep>
//      Spacing between bunches in time in [ns]
//      Default: 10.0
circumference: <circ>
//      Circumference of the synchrotron in [m].
//      Default: 1.0
xfact: <xfact>
//      Bunch by bunch intensity fluctuation in [%]
//      Default: 0
kick: <kick>
//      Initial horizontal momentum kick in units of the rms beam size.
//      Default: 0
load beam from file: <path to file%(Beam,bunch)>
//      Load 6D beam distribution from file.
//      Default: To generate normal distribution numerically.
//      Ex: output/B%ib%i_0.beam
first turn: <turn0>
//      Start the turn counter at this value (useful when reloading a beam)
//      Default: 0
output directory: <path>
//      Path to directory where output files should be stored.
//      No default
copy files to output directory: <integer>
//      Boolean (integer) for whether or not to store the 4 input files in the output directory.
//      Default: 0
dump beam parameter to file: <filename%(Beam,bunch)>
//      File-format of beam parameters dumped each turn.
//      Including %i%i to take the beam and bunch number.
//      Default: B%ib%i.bparam (first number is beam number, second is bunch number)
dump beam to file: <filename%(Beam,bunch,turn)> <turnFreq> <npart>
//      Dump to a given file-format the 6D phase space of <npart> particles every <turnFreq> turn.
//      Default: To not dump
dump beam distribution: <coordinate> <filename%(Beam,bunch,turn)> <turnFreq (optional)>
//      Dump histogram of given <coordinate> to a given file-format every <turnFreq> turn (default: 1)
//      Default: To not dump
dump transverse beam distribution: <filename%(Beam,bunch,turn)> <turnFreq (optional)>
//      Dump 2D histogram in (Jx,Jy) space to file every <turnFreq> turn (default: 1)
//      Default: To not dump
//      Ex: B%ib%i_t%i.tdistr 10000
dump slices position: <filename%(Beam,bunch,coordinate)> <nslice> <turnFreq> <nring (optional)>
//      Dump longitudinal 2D histogram of all coordinates,
//      <nslice> azimuthal sectors and <nring> rings everyt <turnFreq> turn.
//      Default: To not dump
//      Ex: B%ib%i_%c.spos 20 10000 20
dump bunch collision schedule: <filename(Beam,bunch)>
//      Filename of where to dump the bunch collision schedule. Useful to check input.
//      Default: To not dump
//      Ex: B%ib%i.collsched
walltime: <wtime>
//      Stop the simulation after <wtime> hours if it has not completed.
//      Default: To continue until completion.
test particle file: <filename%(Beam,bunch)>
//      Filename where to dump test particles
//      Default: To not dump
//      Ex: B%ib%i.testpart
test particle amplitude: <from> <to> <stepcount>
//      Initial amplitudes of test particles
//      Default: 0.0 12.0 None
//      Ex: 0.0 12.0 100
test particle angle:  <from> <to> <stepcount>
//      Initial angles of test particles
//      Default: 0.0 6.2830 60
feedback table files: <path to feedback table>
//      Tables used by ADT implementation (action code 14.3 modes 10-11)
//      No default
wake table files: <path to wake table B1> <path to wake table B2>
//      Paths to wake function tables, used by action 12
//      No default
wake length: <wake length>
//      Number of turns to store the wakefields for
//      No default
beta for impedance kick: <hor. beta> <ver. beta (optional)>
//      Transverse beta functions at location of impedance kick in [m]
//      Default: 0.55 0.55
instability threshold: <thresh>
//      Abort the simulation if the centroid moves outside <thresh> in [nb of sigma]
//      No default
//      Ex: 0.5
FPPS radial grid size: <nb trans. radii>
//      Radial grid size in action code 4 (beam-beam)
//      Default: 0
FPPS angular grid size: <nb trans. angles>
//      Angular grid size in action code 4 (beam-beam)
//      Default: 0
luminosity computation: <filename%(bunch in B1, bunch in B2, IP)> <nsigma> <gridsize x> gridsize y>
//      Calculate luminosity between two bunches at a given IP with action code 17.
//      Default: To not calculate the luminosity
//      Ex: B1b%i-B2b%i_IP%i.lumi 12 800 800
external field: <path to external field file>
//      Used by action code 16 (external field)
momentum compaction factor: <momentum compaction factor>
//      Used by action code 15 (synchrotron radiation)
//      Default: 0
average horizontal beta: <avg hor. beta>
//      Used by action code 15 (synchrotron radiation)
//      Default: 0
average vertical beta: <avg ver. beta>
//      Used by action code 15 (synchrotron radiation)
//      Default: 0
synchrotron radiation table: <path to synchrad table>
//      Used by action code 15 (synchrotron radiation)
//      No default
omega RF: <omegaRF>
//      Unknown use...
//      Default: 0
voltRF: <voltRF>
//      Unknown use...
//      Default: 0
```



## Beam files

Each beam is defined as a sequence of trains/groups of consecutive bunches, separated by sets of empty bunch slots.
A beam file looks like:

    #Number of groups
    N   
    n1 <flag 1> n2 <flag 2> ... nN <flag N>

where

    // N: The number of groups
    // ni: Number of bunch slots in group i
    // <flag i>  if 1: all bunch slots in group i are filled with bunches
                 else: all bunch slots in group i are empty

Note that there must be equal number of bunch slots in both beams.

### Example
A beam consisting of one bunch at bunch slot 0 and one bunch at bunch slot 5, out of 20 possible bunch slots (40 positions in the [collider model](physics.md#collider-model)), can be initialized as:

    #Number of groups
    4
    1 1 4 0 1 1 14 0


## Action file

The [actions](input_actions.md) are written in the action file, line by line, as:

    P AC I1 I2 ...
        // P:    Position of the action
        // AC:   Action code
        // Ii:   Extra input number i, specific for the action code

The actions are read line by line, overwriting what has been given before. In other words, if an action file looks like:

    0 1 ...
    0 2 ...

it will be set up a simulation with AC=2 on position 0. The line with AC=1 will be neglected.

Note that the number of positions in the [collider model](physics.md#collider-model) is defined as twice the number of bunch slots. Hence, if you have e.g. $10$ bunch slots according to the [beam files](input_files.md#beam-files), you cannot put an action on position $\texttt{P}\geq20$ (start count at $0$).
