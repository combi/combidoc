# File output

$\texttt{COMBI}$ has been implemented to be able to save various types of information to file.
This can be enabled with the "dump \*\*\* to file " lines in the configuration file.

  * Beam parameter\*
  * Beam\*
  * Beam distribution
  * Transverse beam distribution
  * Slices position
  * Bunch collision schedule

\* These file types are explained more below.


## Beam parameter file
This information consists of 18 macroscopic numbers that are stored after each turn. 1 turn per line.

```
1.<X>
2.sqrt<X^2>
3.<Y>
4.sqrt<Y^2>
5.<PX>
6.sqrt<PX^2>
7.<PY>
8.sqrt<PY^2>
9.rel. emit X
10.rel. emit Y
11.bunch intensity
12.<z>
13.sqrt<z^2>
14.<dp/p>
15.sqrt<(dp/p)^2>
16.Rel. emit S
17.sqrt(<XPX> - <X><PX>)
18.sqrt(<YPY> - <Y><PY>)
```

## Beam file
Store the 6D phase space of a certain number of particles (+ whether or not the particles are lost) at a given turn intervals.
For each particle, store the following information:
```
1.X
2.PX
3.Y
4.PY
5.z (ns)
6.Dp/p
7.(!)lost / intensity fluctuation
```

Be careful, this will quickly require a lot of disk space.
