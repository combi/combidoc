# Terminal output

When you launch a $\texttt{COMBI}$ simulation, a lot of information is automatically printed to the terminal. Both simple information and several lists of the initial placement of bunches and actions that are printed to help you check how the initialization worked.

The output of a test simulation will be explained here in chronological order.
There are 2 bunches per beam, but 10 bunch positions, meaning that there are 20 total positions (from 0 to 19).
There are both beam-beam interactions (AC=10,20), 6D phase advance (AC=3), feedback (AC=14).

## Simple information

Whether or not the simulation uses a random seed or not.
```
Using current time as random seed
```
Test of the $\texttt{MPI}$ implementation. How many bunches can be kept per process.
```
MAX MPI_TAG:2147483647 -> MAX bunch Per Proc:13377
```
Modification of the bunch length to get agreement between the bunch spacing and number of bunch slots and the circumference.
```
 Circumference has been changed 26658.88m->26711.51m. Corresponds to 3564 slots (<=3564?) spaced by 25ns.
```
Check of the input BEAM files and where the bunches are placed.
```
 fill1: ./input/1test.B1
 fill2: ./input/1test.B2
 Number of groups beam1:            2
           2           1           8           0
           1
 Number of bunch positions found beam1:           10
 Number of bunches found beam1:            2
 Number of groups beam2:            2
           2           1           8           0
           1
 Number of bunch positions found beam2:           10
 Number of bunches found beam2:            2
 Second bunch count:            2           2
           1          19
           1           3
 POS:      1         1      1
 POS:      3         0      2
 POS:     19         2      0
```
Other simple-to-understand information.
```
revtime=89100.00, nslot=3564, turn0=0

-Initialisation of bunches on the processes:
-  Number of positions=20
-  Number of turns=100
-  Number of active processes=1
-# bunches in the beams:
-  nBunchB1=2
-  nBunchB2=2
-  Max number of bunches on a process = 4
```


## List of initial placement of bunches and actions
For each position where there is something, the bunch number (>0) is printed if there is a bunch, and the action code (>0) is printed with extra parameters if there is an action.
```
*Initial position of Bunches and position of Actions in a Machine with 20 positions:
*inPos/pos : B1bn B2bn   Action
*        0 :    1    1   20(1.00E+00,1.00E+00)
*        1 :    0    0   10(1.00E+00,2.00E+00,0.00E+00)
*        2 :    0    2   0()
*        3 :    0    0   3(3.10E-01,3.20E-01,1.91E-03,3.10E-01,3.20E-01,1.91E-03,0.00E+00,0.00E+00,0.00E+00,0.00E+00)
*        4 :    0    0   14(0.00E+00,1.00E-02,1.00E-02,1.00E-02,1.00E-02)
*       18 :    2    0   0()
*       19 :    0    0   10(1.00E+00,2.00E+00,0.00E+00)
```
There are long-range beam-beam interactions at positions 1 and 19, these are the two positions closest to position 0 where there is a beam-beam interaction.


## List of the bunch placement in the MPI hierarchy
For each position: the bunch number (>0), the rank (which $\texttt{MPI}$ process) of the bunch (>-1), and the bunch number on that process (>-1).
This is to help debug to see where the bunches are.
```
¤B1,B2 = Bunches with their initial position, number in the beam, ranks and number on the process:
¤ inPos : B1bn rank iBunchOnProc| B2bn rank iBunchOnProc
¤     0 :    1    0      0      |    1    0      2
¤     2 :    0   -1     -1      |    2    0      3
¤    18 :    2    0      1      |    0   -1     -1
```

## List of the bunch placement in the collider model and actual collider.
For each bunch in each beam: the position in the collider model (in this case from 0 to 19) and the bunch slot in the actual collider (from 0 to 3563 in the LHC).
The bunches in beam 1 after the first bunch are occupying the highest position and bunch slot numbers (as shown in [physics](physics.md)).
```
~B1pos,B2pos = Bunches initial positions (only for existing bunches, -1 signifies 'no bunch')
~ ibunch: B1p B1slot   |   B2p B2slot
~     1 :   0      0   |     0      0
~     2 :  18   3563   |     2      1
```

## List of the actions per bunch
For each bunch, print which action is to be calculated at which position in the collider model, and with which bunch it is supposed to interact. This is good to check if i.e. the beam-beam interactions are positioned correctly.
```
A: All actions in order per bunch.
   ACatP(bXrXpXsX):
       AC= Action code
       P = position of action
       b = opposite bunch
       r = rank (of opposite bunch)
       p = initial position of opposite bunch
       s = flag for sending (1:send, 0:receive)
A: B1b1 8 - Action (with)  20at0(b1r0p0s1)    | 20at0(b1r0p0s0)    | 10at1(b2r0p2s1)    | 10at1(b2r0p2s0)    |  3at3(alone)       | 14at4(alone)       | 10at19(alone)      | 10at19(alone)      |
A: B1b2 8 - Action (with)  10at19(b1r0p0s1)   | 10at19(b1r0p0s0)   | 20at0(b2r0p2s1)    | 20at0(b2r0p2s0)    | 10at1(alone)       | 10at1(alone)       |  3at3(alone)       | 14at4(alone)       |
A: B2b1 8 - Action (with)  20at0(b1r0p0s1)    | 20at0(b1r0p0s0)    | 10at19(b2r0p18s1)  | 10at19(b2r0p18s0)  | 14at4(alone)       |  3at3(alone)       | 10at1(alone)       | 10at1(alone)       |
A: B2b2 8 - Action (with)  10at1(b1r0p0s1)    | 10at1(b1r0p0s0)    | 20at0(b2r0p18s1)   | 20at0(b2r0p18s0)   | 10at19(alone)      | 10at19(alone)      | 14at4(alone)       |  3at3(alone)       |
```
In this example, B1b1 interacts with B2b1 at position 0 and B2b2 at position 1, but there is no bunch to collide with at position 19.
On the other hand, B1b2 interacts with B2b2 at position 0 and B2b1 at position 19, but there is no bunch to collide with at  position 1.
