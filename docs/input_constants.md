
Quite a lot of constants are defined in $\texttt{src/c/const.h}$. These are available for the source files in $\texttt{C}$ only.
```C
#define NPARAM 18                   // Number of parameters dumped each turn
#define NCOORD 7                    // Number of coordinates in the beam (6D + charge)
#define NSLOTMAX 3564               // Max number of bunch slots in the machine
#define NPOSMAX 2*NSLOTMAX          // Max number of positions where actions can be
#define PI 3.14159265358979323846   // PI
const double SPEEDOFLIGHT =  299792458;

#define NCOORDSLICE_1 8             // Number of coordinates for which to calculate first order moments
#define NCOORDSLICE_2 11            // Number of coordinates for which to calculate second order moments

#define PROGRESSMETER 1             // If "true", show progress during simulation
#define VERSION "2.0"               // Version of master-worker COMBI parallelization - discontinued
#define pVERSION "1.0"              // Version of pipelined COMBI paralllelization

#define RNDSEED 1                   // If "true", seed the random number generator based on date

#define ABORTACTION 84543           // Action code to abort the simulation in discontinued version

#define DEBUG 0                     // Increase the number to print increasing levels of debug while running
#define WALLTIMING 1                // If "true", take the time of the simulation
#define BOOL_WRITE true             // If true, will write various output to files
#define NTURN_SENDPASSAGERECORD 2   // Number of turns to send passagerecord to other bunches in the same beam.
#define ABORT_DELAY 3               // Number of turns delay from instability is detected until the simulation should be aborted
#define SAVE_NWNOISE 0              // If "true", save non-white noise produced by action code 7.5
```

The grid for the HFMM beam-beam action (action code=2) is defined in $\texttt{src/c/pipehelper.h}$

```C
#define NXG 100  
#define NYG 100
#define NGRID ((2*NXG+1)*(2*NYG+1))
```
