# Action codes

## Summary

List of actions implemented (more details given below):

**Beam-beam (BB)**:

- AC=2: Head-on BB using HFMM
- AC=4: Head-on BB using FPPS
- AC=20: Head-on BB using soft-Gaussian method, without crossing angle
- AC=5: BB using soft-Gaussian method, 6D
- AC=50: BB using soft-Gaussian method, Frozen 6D
- AC=10: Long-range BB using soft-Gaussian method, 4D
- AC=100: Long-range BB using soft-Gaussian method, Frozen 4D

**Others**:

- AC=0: Empty
- AC=1: Synchrotron motion (use preferrably AC=3)
- AC=3: 6D phase advance including linear chromaticity
- AC=6: Thin quadrupole magnet
- AC=7: Noise, both coherent and incoherent
- AC=8: Beam transfer function
- AC=11: Collimator
- AC=12: Impedance/Wakefields
- AC=13: Octupole magnet
- AC=14: Transverse damper
- AC=15: Arc synchrotron radiation
- AC=16: E-lens
- AC=17: Luminosity

<!-- ## All action codes -->
Here follows a short list of all action codes and parameters.
Further description of usage is given below.
The 'P' is a placeholder for which position the action will be at.
```
P 0
P 1 QSB1 QSB2
P 2 IntensityScale Flag_PiHalf
P 3 QHB1 QVB1 QSB1 QHB2 QHB2 QSB2 DQHB1 DQVB1 DQHB2 DQVB2
P 4 IntensityScale Flag_PiHalf
P 5 IntensityScale Flag_PiHalf FullXingAngle XingPlane FullSep SepAngle NSlice Flag_LinKick
P 6 QuadStrength TiltAngle Beam
P 7 1 AmplH AmplV
P 7 2 AmplH AmplV FreqH FreqV CorrTimeH CorrTimeV
P 7 3 AmplH AmplV
P 7 4 AmplH AmplV
P 7 5 Flag_Corr TperList Type Misc1 Misc2 AmpHB1 AmpVB1 AmpHB2 AmpVB2
P 7 6 AmplH AmplV
P 7 7 AmplH ReQH ImQH AmplV ReQV ImQV
P 8 Flag_H Flag_V FreqStart FreqStop FreqStep Turn0 TurnsPerStep TurnsPerPause AmplMin AmplMax FreqAmplMin
P 10 SepPlane Sep Flag_DipoleKick
P 11 HorUp HorDown VerUp VerDown LongUp LongDown
P 12 NSlice B1Scale B1FileNumber B2Scale B2FileNumber
P 13 dQH_dJH dQH_dJV dQV_dJH dQV_dJV
P 14 0 GainHB1 GainVB1 GainHB2 GainVB2
P 14 1 GainHB1 GainVB1 GainHB2 GainVB2 ThreshHB1 ThreshVB1 ThreshHB2 ThreshVB2
P 14 2 GainHB1 GainVB1 GainHB2 GainVB2 NSlice Omega
P 14 3 Mode Delay CutuffHB1 GainHB1 QHB1 DeltaHB1 (CutuffVB1 GainVB1 QVB1 DeltaVB1 (CutuffHB2 GainHB2 QHB2 DeltaHB2 CutuffVB2 GainVB2 QVB2 DeltaVB2))
P 14 4 RGainHB1 RGainVB1 RGainHB2 RGainVB2
P 15 1 BendingRadius Fraction
P 15 2 i2 i3 i5 Fraction
P 15 3 NormEquEmit Tau Fraction
P 16 BetaElensX BetaElensY BetaElectron DispX DispY DispR Nr
P 17
P 20 IntensityScale Flag_PiHalf
P 50 IntensityScale Flag_PiHalf FullXingAngle XingPlane FullSep SepAngle NSlice Flag_LinKick
P 100 SepPlane Sep Intensity
```



## AC=0: Empty
Does nothing.
Line in action file:
```
P 0
```
Example line at position 0
```
0 0
```

## AC=1: Synchrotron motion (deprecated use preferrably AC=3)

Performs synchrotron motion in the longitudinal plane
```
P 1 QSB1 QSB2
// QSB1: Synchtrotron tune of beam 1
// QSB2: Synchtrotron tune of beam 2
```
Example at position 0
```
0 1 0.002 0.002
```


## AC=2: Head-on BB using HFMM

Beam-beam interaction calculated with the [Hybrid Fast Multipole Method](https://doi.org/10.1103/PhysRevSTAB.4.054402).
```
P 2 IntensityScale Flag_PiHalf
// IntensityScale: Scale intensity relative to value given in the configuration file.
// Flag_PiHalf:    If not 0, rotate transverse phase space by pi/2 before and after
```
Example at position 0 without phase space rotation:
```
0 2 1 0
```

## AC=3: 6D phase advance including linear chromaticity

Betatron and synchrotron motion, including linear chromaticity.
```
P 3 QHB1 QVB1 QSB1 QHB2 QHB2 QSB2 DQHB1 DQVB1 DQHB2 DQVB2
// QHBi:  Horizontal tune in beam i
// QVBi:  Vertical tune in beam i
// QSBi:  Synchrotron tune in beam i
// DQHBi: Horizontal chromaticity in beam i
// DQVBi: Vertical chromaticity in beam i
```
Example at position 0 with normal tunes and chromaticity for LHC.
```
0 3 0.31 0.32 0.002 0.31 0.32 0.002 15 15 15 15
```

## AC=4: Head-on BB using FPPS

Beam-beam interaction calculated with the [Fast Polar Poisson Solver](https://cds.cern.ch/record/2102532).
This method was implemented to overcome noise limitations of the HFMM method of action code 2.
```
P 4 IntensityScale Flag_PiHalf
// IntensityScale: Scale intensity relative to value given in the configuration file.
// Flag_PiHalf:    If not 0, rotate transverse phase space by pi/2 before and after
```
Example at position 0 without phase space rotation:
```
0 2 1 0
```

## AC=5: BB using soft-Gaussian method, 6D

Beam-beam interactions calculated with the soft-Gaussian method.
```
P 5 IntensityScale Flag_PiHalf FullXingAngle XingPlane FullSep SepAngle NSlice Flag_LinKick
// IntensityScale: Scale intensity relative to value given in the configuration file.
// Flag_PiHalf:    If not 0, rotate transverse phase space by pi/2 before and after
// FullXingAngle:  Full crossing angle in [murad]
// XingPlane:      Given in [rad], H=0, V=pi/2  
// FullSep:        Separation between beams in beam sigma
// SepAngle:       Angle of separation in [rad]. H=0, V=pi/2
// NSlice:         Number of longitudinal slices to cut the beam into.
// Flag_LinKick:   If 1, use a linear map. Else, use nonlinear map.
```
Example at position 0 with full vertical crossing angle of 300 murad and a separation of 1 sigma horizontally, calculated with 1000 slices and nonlinear kicks
```
0 5 1 0 300 1.570796 1 0 1000 0
```

## AC=6: Thin quadrupole magnet

Linear transfer map of a thin quadrupole magnet .
```
P 6 QuadStrength TiltAngle Beam
// QuadStrength : Often denoted k
// TiltAngle    : Tilt angle in radians. 0: p_x+=k*x. pi/4: p_x+=k*y.
// Beam         : 1 or 2 (Optional). If given, apply the quadrupole kick only to this beam.
```
Example at position 0 with zero tilt angle, applied to both beams with a quadrupole strength of 0.0001.
```
0 6 0.0001 0
```
## AC=7: Noise, both coherent and incoherent

Noise. Several types.

1: White coherent noise. Drawn from a square distribution
```
P 7 1 AmplH AmplV
// AmplH: Noise amplitude in the horizontal plane (in beam sigma). std=Ampl/sqrt(3)
// AmplV: Noise amplitude in the vertical plane (in beam sigma). std=Ampl/sqrt(3)
```

2: Sinusoidal coherent noise with finite correlation. Kick = Ampl$\times$sin($2\pi\times$Freq$\times$T) - T=turn
```
P 7 2 AmplH AmplV FreqH FreqV CorrTimeH CorrTimeV
// AmplH: Noise amplitude (in beam sigma) in the horizontal plane.
// AmplV: Noise amplitude (in beam sigma) in the vertical plane.
// FreqH: Frequency in units of revolution frequency in the horizontal plane.
// FreqV: Frequency in units of revolution frequency in the vertical plane.
// CorrTimeH: Correlation time (in turns) in the horizontal plane.
// CorrTimeV: Correlation time (in turns) in the vertical plane.
```

3: Quadrupole white coherent noise (proportional to the transverse position in that plane)
```
P 7 3 AmplH AmplV
// AmplH: Noise amplitude in the horizontal plane (in beam sigma). std=Ampl/sqrt(3)
// AmplV: Noise amplitude in the vertical plane (in beam sigma). std=Ampl/sqrt(3)
```

4: Incoherent noise. Drawn from a Gaussian distribution
```
P 7 4 AmplH AmplV
// AmplH: Noise amplitude (in beam sigma) in the horizontal plane.
// AmplV: Noise amplitude (in beam sigma) in the vertical plane.
```

5: Non-white noise spectrum (correlated between several bunches and turns).  
Check the file src/c/nw_noise.c for what number corresponds to which noise spectrum.  
If Flag_Corr>0, the noise is correlated between all bunch slots (3564 in the LHC). Then it is not possible to generate noise for that many turns simultaneously (~10000), depending on your memory
```
P 7 5 Flag_Corr TperList Type Misc1 Misc2 AmpHB1 AmpVB1 AmpHB2 AmpVB2
// Flag_Corr: If>0, correlate the noise between multiple bunches.  
// TperList : How many turns of noise should be produced simultaneously.
// Type     : Which frequency spectrum should be used.
// Misc1    : Extra input parameter needed for some types.
// Misc2    : Extra input parameter needed for some types.
// AmpHB1   : Noise amplitude (in beam sigma) in the horizontal plane of beam 2.
// AmpVB1   : Noise amplitude (in beam sigma) in the vertical plane of beam 1.
// AmpHB2   : Noise amplitude (in beam sigma) in the horizontal plane of beam 2.
// AmpVB2   : Noise amplitude (in beam sigma) in the vertical plane of beam 2.
```

6: Crab cavity amplitude noise
```
P 7 6 AmplH AmplV
// AmplH: Noise amplitude (in rad/m) in the horizontal plane.
// AmplV: Noise amplitude (in rad/m) in the vertical plane.
```

7: Noise equal to offset of an underdamped stochastic harmonic oscillator USHO
```
P 7 7 AmplH ReQH ImQH AmplV ReQV ImQV
// AmplH: White noise amplitude exciting the USHO in the horizontal plane (in beam sigma).
// ReQH : Real tune of USHO in the horizontal plane.
// ImQH : Imaginary tune of USHO in the horizontal plane.
// AmplV: White noise amplitude exciting the USHO in the vertical plane (in beam sigma).
// ReQV : Real tune of USHO in the vertical plane.
// ImQV : Imaginary tune of USHO in the vertical plane.
```

## AC=8: Beam transfer function

Calculation of the beam transfer function as in LHC operation. The amplitude of excitation is linearly ramped down and up when approaching the expected maximum response to minimise the forced oscillation amplitude)
```
P 8 Flag_H Flag_V FreqStart FreqStop FreqStep Turn0 TurnsPerStep TurnsPerPause AmplMin AmplMax FreqAmplMin
// Flag_H       : If 1: Calculate BTF in the horizontal plane.
// Flag_V       : If 1: Calculate BTF in the vertical plane.
// FreqStart    : Lowest tune of the BTF.
// FreqStop     : Highest tune of the BTF.
// FreqStep     : Step length between discrete BTF tunes.
// Turn0        : At what turn to start calculating the BTF.
// TurnsPerStep : Number of turns of BTF excitation per step.
// TurnsPerPause: Number of turns w/o excitation between steps.
// AmplMin      : Min excitation amplitude.
// AmplMax      : Max excitation amplitude.
// FreqAmplMin  : Tune of minimal excitation amplitude. Gives opportunity to excite the beam less at some tunes.
```
Example at position 0, only for the horizontal plane for tunes between 0.3 and 0.32, starting at turn 1000
```
0 8 1 0 0.30 0.32 0.001 1000 2048 2000 1e-4 1e-3 0.31
```
Disclaimer: These values are not ensured to give a good BTF. Trial will be needed.

## AC=10: Long-range BB using soft-Gaussian method, 4D

Long-range beam-beam interaction calculated with the soft-Gaussian method with a partner bunch.
```
P 10 SepPlane Sep Flag_DipoleKick
// SepPlane         : Separation plane: 1=Horizontal, 2=Vertical.
// Sep              : Separation in rms beam size.
// Flag_DipoleKick  : if 1: remove dipole kick (long-range approx)
```
Example at position 0,  with a separation in the horizontal plane of 10 beam sigma including the dipole kick
```
0 10 1 10 0
```

## AC=11: Collimator

Collimate the beam - consider particles as lost if they go outside a limit.
```
P 11 HorUp HorDown VerUp VerDown LongUp LongDown
// HorUp    : Upper limit in the horizontal plane (in sigma).
// HorDown  : Lower limit in the horizontal plane (in sigma).
// VertUp   : Upper limit in the vertical plane (in sigma).
// VertDown : Lower limit in the vertical plane (in sigma).
// LongUp   : Upper limit in the longitudinal plane (in sigma).
// LongDown : Lower limit in the longitudinal plane (in sigma).
```
The longitudinal collimation is not implemented at the moment...

Example at position 0, which collimates particles outside 10 beam sigma.
```
0 11 10 -10 10 -10 0 0
```

## AC=12: Impedance/Wakefields

Calculate and apply wakefields between all bunches in the same beam.
```
P 12 NSlice B1Scale B1FileNumber B2Scale B2FileNumber
// NSlice       : Number of longitudinal slices to cut the beam into.
// B1Scale      : Artificially scale the wakefield (1 is normal).
// B1FileNumber : Which of the wakefiles given in the configuration file should be used.
// B2Scale      : Artificially scale the wakefield (1 is normal).
// B2FileNumber : Which of the wakefiles given in the configuration file should be used.
```
Example at position 0 where both beams are affected by the normal wakefield from separate files, using 2000 slices
```
0 12 2000 1 0 1 1
```

## AC=13: Octupole magnet

Detuning driven by octupole magnets.
```
P 13  dQH_dJH dQH_dJV dQV_dJH dQV_dJV
// dQH_dJH: Horizontal in-plane detuning coefficient.
// dQH_dJV: Horizontal cross-plane detuning coefficient.
// dQV_dJH: Vertical cross-plane detuning coefficient.
// dQV_dJV: Vertical in-plane detuning coefficient.
```
The detuning coefficients are relative to normalized actions.

Example at position 0 with equal detuning in both planes, similar to the LHC
```
0 13 10e-5 -7e-5 -7e-5 10e-5
```

## AC=14: Transverse damper
Transverse feedback system. Many types.

0: Ideal transverse damper with damping time equal to 2/gain
```
P 14 0 GainHB1 GainVB1 GainHB2 GainVB2
// GainHB1: Gain in the horizontal plane of beam 1.
// GainVB1: Gain in the vertical plane of beam 1.
// GainHB2: Gain in the horizontal plane of beam 2.
// GainVB2: Gain in the vertical plane of beam 1.
```
1: Ideal transverse damper with damping time equal to 2/gain, but only for offsets larger than a threshold. Note that the thresholds are not given in units of beam sigma, but in units of meters.
```
P 14 1 GainHB1 GainVB1 GainHB2 GainVB2 ThreshHB1 ThreshVB1 ThreshHB2 ThreshVB2
// GainHB1: Gain in the horizontal plane of beam 1.
// GainVB1: Gain in the vertical plane of beam 1.
// GainHB2: Gain in the horizontal plane of beam 2.
// GainVB2: Gain in the vertical plane of beam 1.
// ThreshHB1: Threshold in the horizontal plane of beam 1 (real size).
// ThreshVB1: Threshold in the vertical plane of beam 1 (real size).
// ThreshHB2: Threshold in horizontal plane of beam 2 (real size).
// ThreshVB2: Threshold in the vertical plane of beam 2 (real size).
```
2: Single turn ADT (?)
```
P 14 2 GainHB1 GainVB1 GainHB2 GainVB2 NSlice Omega
// GainHB1: Gain in the horizontal plane of beam 1.
// GainVB1: Gain in the vertical plane of beam 1.
// GainHB2: Gain in the horizontal plane of beam 2.
// GainVB2: Gain in the vertical plane of beam 1.
// NSlice : Number of (equidistant) slices.
// Omega  : ?
```
3: Low-bandwidth damper. Damps a bunch based on the measurement of its neighbors as well.  
If submitted with $3+4\times 1$ arguments, the same feedback in all planes of both beams.  
If submitted with $3+4\times 2$ arguments, the same feedback in the horizontal (and vertical) plane of both beams.
```
P 14 3 Mode Delay CutuffHB1 GainHB1 QHB1 DeltaHB1 (CutuffVB1 GainVB1 QVB1 DeltaVB1 (CutuffHB2 GainHB2 QHB2 DeltaHB2 CutuffVB2 GainVB2 QVB2 DeltaVB2))
// Mode     : What kind of low bandwidth feedback to use.
//      = 0 : Extended ideal BW (perfect feedback, but with option of a delay).
//      = 1 : Analog RC feeedback.  
//      = 2 : Symmetric exponential response function.
//      = 3 : Symmetric Gaussian response function.
//      = 4 : Symmetric sinc w/ no window response function.
//      = 10: Digital ADT from table, max 32 neighbors.
//      = 11: Analog ADT from table, including more neighbors.
// Delay    : Number of turns from measurement to feedback.
// Cutuff*B*: Cutoff frequency of the low bandwidth feedback.
// Gain*B*  : Gain.
// Q*B*     : Tune you expect the beam to oscillate with in a given plane.
// Delta*B* : RMS error in beam measurement (in units of beam sigma).
```
4: Ideal transverse REACTIVE damper, which causes a real tune shift with damping time equal to gain$/4\pi$
```
P 14 4 RGainHB1 RGainVB1 RGainHB2 RGainVB2
// RGainHB1: Reactive gain in the horizontal plane of beam 1.
// RGainVB1: Reactive gain in the vertical plane of beam 1.
// RGainHB2: Reactive gain in the horizontal plane of beam 2.
// RGainVB2: Reactive gain in the vertical plane of beam 1.
```

## AC=15: Arc synchrotron radiation

Synchrotron radiation. Several options.

1: regular synchrotron radiation.
```
P 15 1 BendingRadius Fraction
// BendingRadius: Banding radius of the machine in meters.
// Fraction     : Arc length (fraction of the circumference).
```
2: Gaussian noise based on radiation integrals
```
P 15 2 i2 i3 i5 Fraction
// i2       : ?
// i3       : ?
// i5       : ?
// Fraction : Arc length (fraction of the circumference).
```
3: Gaussian noise based on transverse normalized equilibrium emittance and damping time
```
P 15 3 NormEquEmit Tau Fraction
// NormEquEmit  : The physical transverse equilibrium emittance.
// Tau          : The transverse damping time [turn].
// Fraction     : Arc length (fraction of the circumference).
```

## AC=16: E-lens

Fixed field from external file (e-lens)
```
P 16 BetaElensX BetaElensY BetaElectron DispX DispY DispR Nr
// BetaElensX   : ?
// BetaElensY   : ?
// BetaElectron : ?
// DispX        : ?
// DispY        : ?
// DispR        : ?
// Nr           : ?  
```

## AC=17: Luminosity

Calculate luminosity
```
P 17
```

## AC=20: Head-on BB using soft-Gaussian method, without crossing angle

Beam-beam interaction calculated with the soft-Gaussian method with a partner bunch.
```
P 20 IntensityScale Flag_PiHalf
// IntensityScale: Scale intensity relative to value given in the configuration file.
// Flag_PiHalf:    If not 0, rotate transverse phase space by pi/2 before and after
```
Example at position 0 without phase space rotation:
```
0 20 1 0
```

## AC=50: BB using soft-Gaussian method, Frozen 6D

Incoherent beam-beam interaction calculated with the soft-Gaussian method, not requiring a partner.
```
P 50 IntensityScale Flag_PiHalf FullXingAngle XingPlane FullSep SepAngle NSlice Flag_LinKick
// IntensityScale: Scale intensity relative to value given in the configuration file.
// Flag_PiHalf:    If not 0, rotate transverse phase space by pi/2 before and after
// FullXingAngle:  Full crossing angle in [murad]
// XingPlane:      Given in [rad], H=0, V=pi/2  
// FullSep:        Separation between beams in beam sigma
// SepAngle:       Angle of separation in [rad]. H=0, V=pi/2
// NSlice:         Number of longitudinal slices to cut the beam into.
// Flag_LinKick:   If 1, use a linear map. Else, use nonlinear map.
```
Example at position 0 with full vertical crossing angle of 300 murad and a separation of 1 sigma horizontally, calculated with 1000 slices and nonlinear kicks
```
0 50 1 0 300 1.570796 1 0 1000 0
```

## AC=100: Incoherent long-range BB, Frozen 4D

Incoherent long-range beam-beam interaction calculated without a partner bunch.
```
P 100 SepPlane Sep Intensity
// SepPlane : Separation plane: 1=Horizontal, 2=Vertical.
// Sep      : Separation in rms beam size.
// Intensity: Intensity of the fictive other bunch.
```
Example at position 0,  with a separation in the horizontal plane of 10 beam sigma and an intensity of 1e11 particles.
```
0 100 1 10 1e11
```
