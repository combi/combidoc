
# How to run a simulation

When running $\texttt{COMBI}$, the only input to the executable is the path to the configuration file (1 of the 4 required [input files](input_files.md))

```bash
# Serially
./COMBIp <path-to-config-file>

# With 2 processes using MPI
mpirun -np 2 <path-to-config-file>
```
