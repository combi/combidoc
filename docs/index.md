
$\texttt{COMBI}$ is a multiparticle tracking code.
The acronym "$\texttt{COMBI}$" stands for "COherent Multibunch Beam-beam Interactions". The code simulates interactions of two counter rotating beams, which each can consist of several bunches. Several “actions” can be included in the simulation: linear transport, head-on and long-range beam-beam interactions, various noise sources, collimators, impedances, linear detuning, transverse feedback systems, synctrotron radiation, and more.

This documentation explains the [physics](physics.md) $\texttt{COMBI}$ can simulate and how the code is [parallelized](parallelization.md) to exploit modern-day clusters, using both multi-threading and multiple processes.

How to setup and run a simulation is explained in [how to run a simulation](input_howto.md). This requires 4 [input files](input_files.md).

How to understand the results of a simulation is explained in [terminal output](output_terminal.md) and [output files](output_file).


## Web resources
   * [GitLab repository](https://gitlab.cern.ch/sfuruset/combip)
   * [Recent paper on the pipeline parallelization scheme (2019)](https://doi.org/10.1016/j.cpc.2019.06.006)


## Technical information
   * **Programming Languages used for implementation:**
      * $\texttt{FORTRAN}$, $\texttt{C}$, and $\texttt{C++}$
   * **Operating systems:**
      * Tested exclusivey on Linux (Ubuntu 12.04, 16.04, 18.04, SLC 5 and CENTOS7)
   * **Other prerequisites:**
      * Libraries: fftw, GSL


## Other informations
   * <b>Developed by : </b>CERN
   * <b>License : </b>CERN Copyright
   * **Contact persons** : [Xavier Buffat](http://phonebook.cern.ch/phonebook/#personDetails/?id=699946 "CERN phone book"),
                           [Sondre Vik Furuseth](mailto:s.v.furuseth@gmail.com "email address")
   * **Author of this documentation** :
                           [Sondre Vik Furuseth](mailto:s.v.furuseth@gmail.com "email address")
